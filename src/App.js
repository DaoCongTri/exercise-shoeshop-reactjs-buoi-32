import "./App.css";
import ExShoeShop from "./ExShoeShop/ExShoeShop";
function App() {
  return (
    <div>
      <h1 className="text-center text-white bg-dark p-4">
        Exercise React JS - Buổi 32
      </h1>
      <ExShoeShop />
    </div>
  );
}

export default App;
