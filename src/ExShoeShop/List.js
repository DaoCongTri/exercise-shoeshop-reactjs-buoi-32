import React, { Component } from "react";
import ItemShoe from "./ItemShoe";
export default class List extends Component {
  renderListShoe() {
    return this.props.list.map((item, index) => {
      return (
        <ItemShoe
          key={index}
          data={item}
          handleChangeDetail={this.props.handleChangeDetail}
          handleAddToCart={this.props.handleAddToCart}
        />
      );
    });
  }
  render() {
    return (
      <div className="col-6 p-5">
        <h1 className="text-center">Shoes Product</h1>
        <div className="row">{this.renderListShoe()}</div>
      </div>
    );
  }
}
