import React, { Component } from "react";
import { listArr } from "./data";
import List from "./List";
import Detail from "./Detail";
import Cart from "./Cart";
export default class ExShoeShop extends Component {
  state = {
    listArr,
    detail: listArr[0],
    cart: [],
  };
  handleChangeDetail = (item) => {
    this.setState({ detail: item });
  };
  handleAddToCart = (item) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((itemShoe) => itemShoe.id === item.id);
    if (index === -1) {
      cloneCart.push({ ...item, quantity: 1 });
    } else {
      cloneCart[index].quantity++;
    }
    this.setState({ cart: cloneCart });
  };
  handleDelete = (idShoe) => {
    this.setState({
      cart: this.state.cart.filter((item) => item.id !== idShoe),
    });
  };
  handleChangeQuantity = (idShoe, option) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => item.id === idShoe);
    if (option === "Up") {
      cloneCart[index].quantity++;
    } else {
      cloneCart[index].quantity--;
    }
    if (cloneCart[index].quantity === 0) {
      cloneCart.splice(index, 1);
    }
    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div className="container">
        <div className="row">
          <List
            handleChangeDetail={this.handleChangeDetail}
            list={this.state.listArr}
            handleAddToCart={this.handleAddToCart}
          />
          <Cart
            cart={this.state.cart}
            handleDelete={this.handleDelete}
            handleChangeQuantity={this.handleChangeQuantity}
          />
        </div>
        <Detail detail={this.state.detail} />
      </div>
    );
  }
}
