import React, { Component } from "react";

export default class Cart extends Component {
  render() {
    let { cart, handleDelete, handleChangeQuantity } = this.props;
    return (
      <div className="col-6 p-5">
        <h1 className="text-center">Cart</h1>
        <table class="table">
          <thead>
            <tr>
              <th>Image</th>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Total</th>
              <th>Section</th>
            </tr>
          </thead>
          <tbody>
            {cart.map((item, index) => {
              let { id, name, image, price, quantity } = item;
              return (
                <tr key={index}>
                  <td>
                    <img src={image} width={100} height={100} alt="" />
                  </td>
                  <td>{name}</td>
                  <td>{price}</td>
                  <td>
                    <button
                      onClick={() => {
                        handleChangeQuantity(id, "Down");
                      }}
                    >
                      -
                    </button>
                    {quantity}
                    <button
                      onClick={() => {
                        handleChangeQuantity(id, "Up");
                      }}
                    >
                      +
                    </button>
                  </td>
                  <td>{price * quantity}</td>
                  <td>
                    <button
                      className="btn btn-danger"
                      onClick={() => {
                        handleDelete(id);
                      }}
                    >
                      X
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
