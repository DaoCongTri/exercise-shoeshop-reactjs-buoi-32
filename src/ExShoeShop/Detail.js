import React, { Component } from "react";
export default class Detail extends Component {
  render() {
    let { image, name, price, description, quantity } = this.props.detail;
    return (
      <div className="container">
        <h1 className="text-center text-white bg-dark p-3 mt-5">
          Detail Shoes
        </h1>
        <div className="row">
          <div className="col-8">
            <img src={image} alt="" />
          </div>
          <div className="col-4 pt-5">
            <p>Name: {name}</p>
            <p>Price: {price}</p>
            <p>Description: {description}</p>
            <p>Stock: {quantity}</p>
          </div>
        </div>
      </div>
    );
  }
}
