import React, { Component } from "react";
export default class ItemShoe extends Component {
  render() {
    let { data, handleChangeDetail, handleAddToCart } = this.props;
    let { name, price, image } = data;
    return (
      <div className="col-4 pt-4">
        <div className="card text-left">
          <img
            className="card-img"
            src={image}
            alt=""
            width={200}
            height={150}
          />
          <div className="card-body" style={{ height: 150 }}>
            <p>{name}</p>
            <p>{price} $</p>
          </div>
          <button
            className="btn btn-secondary"
            onClick={() => {
              handleChangeDetail(data);
            }}
          >
            View more
          </button>
          <button
            className="btn btn-success"
            onClick={() => {
              handleAddToCart(data);
            }}
          >
            Add to cart
          </button>
        </div>
      </div>
    );
  }
}
